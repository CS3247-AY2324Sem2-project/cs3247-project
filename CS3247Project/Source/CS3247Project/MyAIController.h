// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "MyAIController.generated.h"

class UBlackboardComponent;
class UBehaviorTreeComponent;

/**
 * 
 */
UCLASS()
class CS3247PROJECT_API AMyAIController : public AAIController
{
	GENERATED_BODY()
	
private:
	UBlackboardComponent* BlackboardComp;
	UBehaviorTreeComponent* BehaviorComp;

public:

	// Constructor
	AMyAIController();

	// Blackboard Key
	UPROPERTY(EditDefaultsOnly, Category = "AI")
		FName BlackboardKey = "Target";

	// Executes right when the controller possesses a Pawn
	virtual void OnPossess(APawn* pawn) override;

	// Sets the sensed target in the blackboard
	void SetSeenTarget(APawn* pawn);
};
