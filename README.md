# Dreamscape

A fast-paced parkour game where you dash and zip to the goal as quickly as possible.

## How to Play

You can download the executable from [itch.io](https://reneeyeow02.itch.io/dreamscape).